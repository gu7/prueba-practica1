<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListaCursosController extends AbstractController
{
    #[Route('/lista/cursos', name: 'app_lista_cursos')]
    public function index(): Response
    {
        return $this->render('lista_cursos/index.html.twig', [
            'controller_name' => 'ListaCursosController',
        ]);
    }
}
